from os import environ
from pathlib import Path

DEBUG: bool = False
BASE_DIR: Path = Path(__file__).parent.parent
TIMEZONE: str = 'Europe/Moscow'

STUDENT_LOGIN: str = environ.get('STUDENT_LOGIN', '')
STUDENT_PASSWORD: str = environ.get('STUDENT_PASSWORD', '')
