from typing import (List, Optional)

from bs4 import (BeautifulSoup, Tag)
from selenium.webdriver.phantomjs.webdriver import WebDriver

import settings
from apps.faculty.dto import (FacultyInfoDto, EducationFormDto, EducationGroupDto)


def get_faculty_name_and_id() -> List[FacultyInfoDto]:
    """Метод для получения информации о факультетах"""

    def _clean_faculty_info(faculty_web_element_: Tag) -> FacultyInfoDto:
        faculty_id: str = faculty_web_element_.attrs['id'].replace('bt', '')
        return FacultyInfoDto(name=faculty_web_element_.text, id=int(faculty_id))

    with WebDriver(settings.BASE_DIR / 'selenium' / 'drivers' / 'windows' / 'phantomjs.exe') as web_:
        web_.get('https://tt.chuvsu.ru')
        # нажимаем "Войти как гость"
        web_.find_element_by_name(name='guest').click()
        # находим элемент с таблицей факультетов
        bs = BeautifulSoup(web_.page_source, 'lxml')
        faculty_web_element_list: List[Tag] = bs.find_all('div', {'class': 'facbut'})
    return [
        _clean_faculty_info(faculty_web_element_=faculty_web_element)
        for faculty_web_element in faculty_web_element_list
    ]


def get_educational_groups_by_faculty_id(faculty_id: int) -> List[EducationFormDto]:
    """Метод для получения академических групп факультета"""

    def _clean_education_form(education_form_tag: Tag) -> EducationFormDto:
        education_forms = education_form_tag.nextSibling.next.find_all('button', {'class': 'nicebut'})
        education_form_groups: List[EducationGroupDto] = []
        for education_form_group in education_forms:
            education_form_group_degree: Optional[str] = None
            if education_form_group.parent.parent.find('span'):
                education_form_group_degree: str = str(education_form_group.parent.parent.find('span').text)
            elif education_form_group.parent.next.parent.parent.parent.next_element.text:
                education_form_group_degree: str = str(
                    education_form_group.parent.next.parent.parent.parent.next_element.text
                )

            education_form_groups.append(EducationGroupDto(
                id=education_form_group.attrs['id'].replace('gr', ''),
                name=str(education_form_group.text),
                degree=education_form_group_degree,
            ))
        education_form_name: str = education_form_tag.next.replace('обучения', '').strip().title()
        return EducationFormDto(name=education_form_name, groups=education_form_groups, )

    with WebDriver(settings.BASE_DIR / 'selenium' / 'drivers' / 'windows' / 'phantomjs.exe') as web_:
        web_.get(f'https://tt.chuvsu.ru/index/factt/fac/{faculty_id}')
        # нажимаем "Войти как гость"
        web_.find_element_by_name(name='guest').click()
        # нажимаем на кнопку факультета
        web_.find_element_by_id(f'bt{faculty_id}').click()
        bs = BeautifulSoup(web_.page_source, 'lxml')
        return [
            _clean_education_form(education_form_tag=education_form)
            for education_form in bs.find_all('h3')
        ]


__all__ = ('get_faculty_name_and_id', 'get_educational_groups_by_faculty_id',)
