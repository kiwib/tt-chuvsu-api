from typing import List

from apps.faculty.dto import (FacultyInfoDto, EducationFormDto)
from apps.faculty.logic.getters import (
    get_faculty_name_and_id,
    get_educational_groups_by_faculty_id
)


async def faculty_info() -> List[FacultyInfoDto]:
    """Точка доступа для получения списка факультетов"""
    return get_faculty_name_and_id()


async def faculty_groups(faculty_id: int) -> List[EducationFormDto]:
    """Точка доступа для получения академических групп факультета"""
    return get_educational_groups_by_faculty_id(faculty_id=faculty_id)


__all__ = ('faculty_info', 'faculty_groups',)
