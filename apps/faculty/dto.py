from typing import Optional, List

from pydantic import (BaseModel, constr)


class FacultyInfoDto(BaseModel):
    id: int
    name: constr(strip_whitespace=True)


class EducationGroupDto(BaseModel):
    id: int
    name: str
    degree: str


class EducationFormDto(BaseModel):
    name: str
    groups: Optional[List[EducationGroupDto]] = []


__all__ = (
    'FacultyInfoDto',
    'EducationGroupDto',
    'EducationFormDto',
)
