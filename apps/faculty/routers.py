from typing import List

from fastapi import APIRouter

from apps.faculty.dto import (FacultyInfoDto, EducationFormDto)
from apps.faculty.endpoints import (faculty_info, faculty_groups)

router = APIRouter()

router.add_api_route(
    path='/',
    methods=['GET'],
    endpoint=faculty_info,
    name='faculty_info',
    response_model=List[FacultyInfoDto]
)
router.add_api_route(
    path=r'/{faculty_id}/groups',
    methods=['GET'],
    name='faculty_groups',
    endpoint=faculty_groups,
    response_model=List[EducationFormDto]
)

__all__ = ('router',)
