from fastapi import APIRouter

from apps.educational_week.dto import EducationalWeekInfoDto
from apps.educational_week.endpoints import educational_week_info

router = APIRouter()

router.add_api_route(
    path='/info',
    methods=['GET'],
    endpoint=educational_week_info,
    name='educational-week-info',
    status_code=200,
    response_model=EducationalWeekInfoDto,
)

__all__ = ('router',)
