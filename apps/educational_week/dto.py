from pydantic import BaseModel

from apps.educational_week.consts import (
    HUMANIZE_EVEN_EDUCATIONAL_WEEK,
    HUMANIZE_ODD_EDUCATIONAL_WEEK, EVEN_EDUCATIONAL_WEEK_CODE, ODD_EDUCATIONAL_WEEK_CODE, FALL_SEMESTER_HUMANIZE_TYPE,
    FALL_SEMESTER_CODE, SPRING_SEMESTER_HUMANIZE_TYPE, SPRING_SEMESTER_CODE,
)


class EducationalWeekInfoDto(BaseModel):
    parity_symbol: str
    week_number: int
    semester_code: str

    @property
    def parity_humanize(self) -> str:
        parity_symbol_length: int = len(self.parity_symbol)
        if parity_symbol_length == 2:
            return HUMANIZE_EVEN_EDUCATIONAL_WEEK
        elif parity_symbol_length == 1:
            return HUMANIZE_ODD_EDUCATIONAL_WEEK
        raise ValueError('Не удалось определить четность учебной недели')

    @property
    def parity_code(self) -> str:
        parity_symbol_length: int = len(self.parity_symbol)
        if parity_symbol_length == 2:
            return EVEN_EDUCATIONAL_WEEK_CODE
        elif parity_symbol_length == 1:
            return ODD_EDUCATIONAL_WEEK_CODE
        raise ValueError('Не удалось определить четность учебной недели', self.parity_symbol)

    @property
    def semester_humanize(self) -> str:
        if self.semester_code == FALL_SEMESTER_CODE:
            return FALL_SEMESTER_HUMANIZE_TYPE
        elif self.semester_code == SPRING_SEMESTER_CODE:
            return SPRING_SEMESTER_HUMANIZE_TYPE
        raise ValueError('Не удалось определить человекочитаемый формат учебной недели', self.semester_code)


__all__ = ('EducationalWeekInfoDto',)
