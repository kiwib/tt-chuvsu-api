import httpx
from bs4 import BeautifulSoup, Tag

from apps.educational_week.consts import (
    FALL_SEMESTER_TYPE,
    FALL_SEMESTER_CODE,
    SPRING_SEMESTER_TYPE,
    SPRING_SEMESTER_CODE,
)
from apps.educational_week.dto import EducationalWeekInfoDto


async def get_education_week_info() -> EducationalWeekInfoDto:
    """Метод для получения четности недели"""

    def _clean_week_number(week_number_: str) -> int:
        """Метод для очистки и получения номера учебной недели"""
        return int(week_number_.lower().replace('идет', '').strip())

    def _clean_semester_type(semester_type_: str) -> str:
        """Метод для очистки и получения квартала семетра"""
        clean_semester_type: str = semester_type_ \
            .replace('неделя', '') \
            .replace('семестра', '') \
            .lower() \
            .strip()

        if clean_semester_type == FALL_SEMESTER_TYPE:
            return FALL_SEMESTER_CODE
        elif clean_semester_type == SPRING_SEMESTER_TYPE:
            return SPRING_SEMESTER_CODE
        raise ValueError('Не удалось определить тип семестра', clean_semester_type)

    async with httpx.AsyncClient() as client:
        r = await client.get('https://tt.chuvsu.ru/')
        bs = BeautifulSoup(r.text, 'lxml')
        week_info: Tag = bs.find(name='sup').parent
        week_number, week_parity, semester_type = week_info.stripped_strings
        return EducationalWeekInfoDto(
            parity_symbol=week_parity,
            week_number=_clean_week_number(week_number_=week_number),
            semester_code=_clean_semester_type(semester_type_=semester_type)
        )


__all__ = ('get_education_week_info',)
