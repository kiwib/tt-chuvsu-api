from apps.educational_week.dto import EducationalWeekInfoDto
from apps.educational_week.logic.getters import get_education_week_info


async def educational_week_info() -> EducationalWeekInfoDto:
    return await get_education_week_info()


__all__ = ('educational_week_info',)
