from fastapi import APIRouter

from apps.educational_schedule.routers import router as educational_schedule_router
from apps.educational_week.routers import router as educational_week_router
from apps.faculty.routers import router as faculty_router

router = APIRouter()

router.include_router(
    router=educational_week_router,
    prefix='/educational-week',
    tags=['educational_week', ],
)
router.include_router(
    router=faculty_router,
    prefix='/faculty',
    tags=['faculty', ],
)
router.include_router(
    router=educational_schedule_router,
    prefix='/educational-schedule',
    tags=['educational_schedule', ],
)

__all__ = ('router',)
