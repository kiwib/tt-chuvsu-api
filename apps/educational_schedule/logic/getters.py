from typing import List, Optional

from bs4 import (BeautifulSoup)
from loguru import logger
from selenium.webdriver.phantomjs.webdriver import WebDriver

import settings
from apps.educational_schedule.dto import EducationalSessionDto


def get_educational_schedule_info() -> List[EducationalSessionDto]:
    def _clean_educational_session(educational_session_: str) -> Optional[EducationalSessionDto]:
        dirty_educational_session: List[str] = educational_session_ \
            .replace('пара', '') \
            .replace('-', '') \
            .split(' ')
        clean_educational_sessions_: List[str] = list(filter(lambda i: i, dirty_educational_session))
        if not clean_educational_sessions_ or not all(clean_educational_sessions_):
            logger.info('Не удалось обработать элемент', educational_session_)
            return
        session_number, session_start_at, session_end_at = clean_educational_sessions_
        return EducationalSessionDto(
            number=session_number,
            start_at=session_start_at,
            end_at=session_end_at,
        )

    with WebDriver(settings.BASE_DIR / 'selenium' / 'drivers' / 'windows' / 'phantomjs.exe') as web_:
        web_.get('https://tt.chuvsu.ru')
        # нажимаем "Войти как гость"
        web_.find_element_by_name(name='guest').click()
        # находим элемент с таблицей факультетов
        bs = BeautifulSoup(web_.page_source, 'lxml')
        educational_sessions: List[str] = list(
            map(
                lambda i: i.text,
                filter(lambda i: i.text, bs.find('div', {'id': 'idt1'}).parent.parent.parent.children)
            )
        )
        clean_educational_sessions: List[EducationalSessionDto] = [
            _clean_educational_session(educational_session_=educational_session)
            for educational_session in educational_sessions
        ]
        return list(filter(lambda i: i, clean_educational_sessions))


__all__ = ('get_educational_schedule_info',)
