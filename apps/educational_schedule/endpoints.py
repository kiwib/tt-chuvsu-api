from typing import List

from apps.educational_schedule.dto import EducationalSessionDto
from apps.educational_schedule.logic.getters import get_educational_schedule_info


async def educational_schedule_info() -> List[EducationalSessionDto]:
    return get_educational_schedule_info()


__all__ = ('educational_schedule_info',)
