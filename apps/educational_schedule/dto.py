from datetime import time

from pydantic import BaseModel, constr


class EducationalSessionDto(BaseModel):
    number: constr(strip_whitespace=True)
    start_at: constr(strip_whitespace=True)
    end_at: constr(strip_whitespace=True)


__all__ = ('EducationalSessionDto',)
