from typing import List

from fastapi import APIRouter

from apps.educational_schedule.dto import EducationalSessionDto
from apps.educational_schedule.endpoints import educational_schedule_info

router = APIRouter()

router.add_api_route(
    path='/',
    methods=['GET'],
    endpoint=educational_schedule_info,
    name='educational_schedule_info',
    response_model=List[EducationalSessionDto],
)

__all__ = ('router',)
