import uvicorn
from fastapi import FastAPI

import settings
from apps.routers import router as apps_router

app = FastAPI(debug=settings.DEBUG, )
app.include_router(router=apps_router)

if __name__ == "__main__":
    uvicorn.run(app=app)
