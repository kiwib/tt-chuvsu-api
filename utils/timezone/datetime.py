from datetime import datetime
from typing import Optional

import maya
from pendulum.parsing.exceptions import ParserError as PendulumParseError

import settings


def convert_string_date_to_datetime_object(
        datetime_string: str,
        raise_exception: Optional[bool] = False,
) -> Optional[datetime]:
    """Метод для преобразования строки с датой в в объект datetime"""
    try:
        maya_object: maya.MayaDT = maya.parse(string=datetime_string, timezone=settings.TIMEZONE)
    except (ValueError, PendulumParseError) as ex:
        if raise_exception:
            raise ex
        return None
    datetime_object: datetime = maya_object.datetime()
    return datetime_object


__all__ = ('convert_string_date_to_datetime_object',)
