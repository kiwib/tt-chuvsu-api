FROM python:3.9.6-alpine3.13
MAINTAINER Andrey Shaikin <kiwibon@yandex.ru>

ENV PROJECT_DIR=/opt/project \
    POETRY_VERSION=1.0.0 \
    PYTHONUNBUFFERED=1 \
    PORT=8010

RUN true \
    && mkdir -p $PROJECT_DIR \
    && apk upgrade \
    && apk add --no-cache \
        curl \
        gcc \
        openssh \
        git \
        rust \
        openssl-dev \
        musl-dev \
        cargo \
        libffi-dev \
        make \
        libxml2-dev \
        libxslt-dev \
        py-cryptography \
        postgresql-dev \
        libevent-dev \
        build-base

RUN python -m pip install --upgrade pip
RUN pip install "poetry==$POETRY_VERSION"
WORKDIR $PROJECT_DIR
COPY . .

RUN true \
    && poetry config virtualenvs.create false \
    && poetry install --no-dev --no-interaction --no-ansi

EXPOSE $PORT
RUN uvicorn app:app --port=$PORT --reload --workers 2 &
